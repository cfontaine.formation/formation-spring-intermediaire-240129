package fr.dawan.springboot.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dtos.CharteGraphiqueDto;
import fr.dawan.springboot.entities.relation.CharteGraphique;
import fr.dawan.springboot.repositories.CharteGraphiqueRepository;
import fr.dawan.springboot.services.CharteGraphiqueService;

@Service
@Transactional
public class CharteGraphiqueServiceImpl extends GenericServiceImpl<CharteGraphiqueDto, CharteGraphique, Long>
        implements CharteGraphiqueService {

    public CharteGraphiqueServiceImpl(CharteGraphiqueRepository repository, ModelMapper mapper) {
        super(repository, mapper, CharteGraphique.class, CharteGraphiqueDto.class);
    }

    @Override
    public boolean delete(Long id) {
        return ((CharteGraphiqueRepository) repository).removeById(id) != 0;
    }

    @Override
    protected void updateEntity(CharteGraphique cg, CharteGraphiqueDto dto) {
        cg.setCouleur(dto.getCouleur());
        cg.setLogo(dto.getLogo());
    }

}
