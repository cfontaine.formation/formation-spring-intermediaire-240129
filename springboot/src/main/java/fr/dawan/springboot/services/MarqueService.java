package fr.dawan.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dtos.MarqueDto;

public interface MarqueService {

    List<MarqueDto> getAllMarque(Pageable page);
    
    MarqueDto getMarqueById(long id);
    
    List<MarqueDto> getMarqueByNom(String nom);
    
    boolean deleteMarque(long id);
    
    MarqueDto save(MarqueDto marqueDto);
    
    MarqueDto update(MarqueDto marqueDto,long id);
    
}
