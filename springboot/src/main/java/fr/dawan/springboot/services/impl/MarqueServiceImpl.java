package fr.dawan.springboot.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relation.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
//        List<Marque> lst=repository.findAll(page).getContent();
//        List<MarqueDto> lstDto=new ArrayList<>();
//        for(Marque m:lst) {
//           lstDto.add(mapper.map(m, MarqueDto.class));
//        }
//        return lstDto;
        Page<Marque> p = repository.findAll(page);
        return p.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public MarqueDto getMarqueById(long id) {
        Marque m = repository.findById(id).get();
        return mapper.map(m, MarqueDto.class);

    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        List<Marque> lst = repository.findByNom(nom);
        return lst.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public boolean deleteMarque(long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    public MarqueDto save(MarqueDto marqueDto) {
        Marque m = repository.saveAndFlush(mapper.map(marqueDto, Marque.class));
        return mapper.map(m, MarqueDto.class);
    }

    @Override
    public MarqueDto update(MarqueDto marqueDto,long id) {
        Marque m = repository.findById(id).get();
        m.setNom(marqueDto.getNom());
        m.setDateCreation(marqueDto.getDateCreation());
        return mapper.map(repository.saveAndFlush(m), MarqueDto.class);
    }

}
