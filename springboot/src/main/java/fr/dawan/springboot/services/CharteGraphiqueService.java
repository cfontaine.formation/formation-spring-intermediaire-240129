package fr.dawan.springboot.services;

import fr.dawan.springboot.dtos.CharteGraphiqueDto;

public interface CharteGraphiqueService extends GenericService<CharteGraphiqueDto, Long>{
    
}
