package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.entities.relation.Article;
import fr.dawan.springboot.entities.relation.Emballage;
import fr.dawan.springboot.entities.relation.Marque;

@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {

    // sujet find
    List<Article> findByPrix(double prix);

    List<Article> findByPrixLessThan(double prixMax);

    List<Article> findByPrixGreaterThanAndEmballage(double prixMin, Emballage emballage);

    List<Article> findByPrixBetween(double prixMin, double prixMax);

    List<Article> findByEmballageIn(Emballage... emballages);

    List<Article> findByDescriptionLike(String modele);

    List<Article> findByDescriptionIgnoreCase(String description);

    List<Article> findByPrixLessThanOrderByDescriptionDescPrix(double prixMax);

    List<Article> findByMarque(Marque marque);

    List<Article> findByMarqueNom(String nomMarque);

    List<Article> findByMarqueDateCreationAfter(LocalDate creation);

    List<Article> findByFournisseursNom(String nomFournisseur);

    // Limiter le résultat
    Article findTopByOrderByPrixDesc();

    List<Article> findTop5ByOrderByPrix();

    // Sujet exist
    boolean existsByEmballage(Emballage emballage);

    // sujet count
    int countByPrixGreaterThan(double prixMin);

    // sujet delete
    void deleteByMarque(Marque m);

    int removeById(long id);

    Page<Article> findAllBy(Pageable page);

    List<Article> findByPrixGreaterThan(double prix, Pageable page);

    // JPQL
    @Query("SELECT a FROM Article a WHERE a.prix<:prix")
    List<Article> findByPrixLessThanJQPL(@Param("prix") double prixMax);

    @Query("FROM Article a WHERE a.prix<:prixMax")
    List<Article> findByPrixLessThanJQPL2(double prixMax);

    @Query("SELECT a FROM Article a WHERE a.prix<?1 AND a.emballage=?2")
    List<Article> findByPrixLessThanJQPL3(double prixMax, Emballage emballage);

    // Pour @OneToOne et ManyToOne
    @Query("SELECT a FROM Article a Where a.marque.nom=:nomMarque")
    List<Article> findByMarqueNomJPQL(String nomMarque);

    // @ManyToMany
    // @Query("SELECT a FROM Article a Where a.fournisseurs.nom=:nomFournisseur")
    @Query("SELECT a FROM Article a JOIN  a.fournisseurs f WHERE f.nom=:nomFournisseur")
    List<Article> findByFournisseurNomsJPQL(String nomFournisseur);

    // SQL
    @Query(value = "SELECT * FROM articles WHERE prix<:prixMax", nativeQuery = true)
    List<Article> findByPrixLessThanSQL(double prixMax);

    // Procedure stockée
    // Appel explicite
    @Procedure("get_count_by_prix")
    int countArticlePrix(double montant);

    // Appel implicite
    @Procedure
    int get_count_by_prix(@Param("prix") double prix);
}
