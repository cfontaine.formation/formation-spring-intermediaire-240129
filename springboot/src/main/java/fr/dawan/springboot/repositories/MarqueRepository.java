package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.relation.Marque;

// @Repository
public interface MarqueRepository extends JpaRepository<Marque,Long> {

    List<Marque> findByNom(String nom);
    
    int removeById(long id);
}
