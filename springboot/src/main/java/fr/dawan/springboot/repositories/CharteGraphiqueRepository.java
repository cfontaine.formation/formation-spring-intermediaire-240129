package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.relation.CharteGraphique;

public interface CharteGraphiqueRepository extends JpaRepository<CharteGraphique, Long> {

    int removeById(long id);
}
