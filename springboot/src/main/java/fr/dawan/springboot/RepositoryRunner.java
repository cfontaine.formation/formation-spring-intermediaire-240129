package fr.dawan.springboot;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import fr.dawan.springboot.entities.relation.Article;
import fr.dawan.springboot.entities.relation.Emballage;
import fr.dawan.springboot.entities.relation.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
//@Order(1)
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private MarqueRepository marqueRepository;

    @Autowired
    private ArticleRepository articleRepository;
    
    @Autowired
    private MarqueCustomRepository customRepository;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Repository runner");
        List<Marque> marques = marqueRepository.findAll();
        for (Marque m : marques) {
            System.out.println(m);
        }
        System.out.println("----------------------------");
        List<Article> lstA = articleRepository.findByPrix(20);
        lstA.forEach(a -> System.out.println(a));
        System.out.println("----------------------------");
        lstA = articleRepository.findByPrixLessThan(100.0);
        lstA.forEach(a -> System.out.println(a));
        System.out.println("----------------------------");
        lstA = articleRepository.findByPrixGreaterThanAndEmballage(100.0, Emballage.CARTON);
        lstA.forEach(a -> System.out.println(a));
        System.out.println("----------------------------");
        lstA = articleRepository.findByPrixBetween(50.0, 200.0);
        lstA.forEach(a -> System.out.println(a));
        System.out.println("----------------------------");
        lstA = articleRepository.findByEmballageIn(Emballage.PLASTIQUE, Emballage.SANS);
        lstA.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        // lstA=articleRepository.findByDescriptionLike("c____%");
        // lstA=articleRepository.findByDescriptionLike("c____ %");
        lstA = articleRepository.findByDescriptionLike("%O");
        lstA.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        lstA = articleRepository.findByPrixLessThanOrderByDescriptionDescPrix(200.0);
        lstA.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        lstA = articleRepository.findByMarqueNom("Marque A");
        lstA.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        lstA = articleRepository.findByMarqueDateCreationAfter(LocalDate.of(1950, 1, 1));
        lstA.forEach(a -> System.out.println(a));

        System.out.println("----------------------------");
        lstA = articleRepository.findTop5ByOrderByPrix();
        lstA.forEach(a -> System.out.println(a));

        System.out.println(articleRepository.findTopByOrderByPrixDesc());

        System.out.println(articleRepository.existsByEmballage(Emballage.PAPIER));

        System.out.println(articleRepository.countByPrixGreaterThan(200.0));

//        System.out.println(articleRepository.removeById(1L));
//        System.out.println(articleRepository.removeById(1000L));
//        
//        Marque mA=marqueRepository.findById(1L).get();
//        articleRepository.deleteByMarque(mA);
        // Pagination

        Page<Article> p = articleRepository.findAllBy(PageRequest.of(3, 3));
        System.out.println(p.getNumberOfElements());
        System.out.println(p.getSize());
        System.out.println(p.getNumber());
        System.out.println(p.getTotalElements());
        System.out.println(p.getTotalPages());

        p.getContent().forEach(a -> System.out.println(a));

        articleRepository.findByPrixGreaterThan(10.0, PageRequest.of(1, 5)).forEach(a -> System.out.println(a));

        Sort sort = Sort.by("description").and(Sort.by(Direction.DESC, "prix"));

        articleRepository.findByPrixGreaterThan(10.0, PageRequest.of(1, 5, sort)).forEach(a -> System.out.println(a));

        articleRepository.findByPrixGreaterThan(10.0, Pageable.unpaged()).forEach(a -> System.out.println(a));
        
        //JPQL
        articleRepository.findByPrixLessThanJQPL(50.0)
        .forEach(a -> System.out.println(a));
        
        articleRepository.findByPrixLessThanJQPL2(50.0)
        .forEach(a -> System.out.println(a));
        
        articleRepository.findByPrixLessThanJQPL3(100.0,Emballage.PLASTIQUE)
        .forEach(a -> System.out.println(a));
        
        articleRepository.findByMarqueNomJPQL("Marque B")
        .forEach(a -> System.out.println(a));
        
        articleRepository.findByFournisseursNom("Fournisseur 1") 
        .forEach(a -> System.out.println(a));
        
        // SQL
        articleRepository.findByPrixLessThanSQL(50.0)
        .forEach(a -> System.out.println(a));
        
        // Procédure stockée
        System.out.println(articleRepository.countArticlePrix(140.0));
        System.out.println(articleRepository.get_count_by_prix(140.0));
   
        // Repository personalisé
        customRepository.findBy("Marque A", null).forEach(m -> System.out.println(m));
        
        customRepository.findBy(null, LocalDate.of(2000, 4, 1)).forEach(m -> System.out.println(m));
        
        customRepository.findBy("Marque D", LocalDate.of(2000, 4, 1)).forEach(m -> System.out.println(m));
      
        // Auditing
        Marque mf=new Marque();
        mf.setNom("Marque F");
        mf.setDateCreation(LocalDate.of(2015,6,13));
        marqueRepository.saveAndFlush(mf);
        
        Marque  mf2=marqueRepository.findById(8L).get();
        mf2.setNom("Marque F2");
        marqueRepository.saveAndFlush(mf2);
    }

}
