package fr.dawan.springboot;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

//@Component
//@Order(2)
public class ServiceRunner implements CommandLineRunner {
    
//    @Autowired
//    private ArticleRepository repository;
//    
//    @Autowired
//    private MarqueRepository marqueRepository;
    
    @Autowired
    private MarqueService service; 
    
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Service Runner");
//        Article a=repository.findById(4L).get();
//        
//        ModelMapper mapper=new ModelMapper();
//        ArticleDto dto=mapper.map(a, ArticleDto.class);
//        System.out.println(dto);
//        
//        mapper.typeMap(Article.class,ArticleDto2.class).addMappings(
//                 m -> m.map(src-> src.getMarque().getNom(), 
//                          (dest,v)-> dest.setIntituleMarque((String)v))
//                );
//        ArticleDto2 dto2=mapper.map(a, ArticleDto2.class);
//        System.out.println(dto2);
//        
//        Provider<Marque> provider= r -> marqueRepository.findByNom(((String)r.getSource())).get(0);
//        mapper.typeMap(ArticleDto2.class, Article.class).addMappings(
//                m -> m.with(provider).map(src -> src.getIntituleMarque(),
//                        (dest,v) -> dest.setMarque((Marque)v)));
//        Article a2=mapper.map(dto2,Article.class);
//        System.out.println(a2);
//        System.out.println(a2.getMarque());
        
        // Service
        service.getAllMarque(Pageable.unpaged()).forEach(m -> System.out.println(m));
        System.out.println("-------------------------------");
        MarqueDto dto=new MarqueDto(0,"Marque M",LocalDate.of(1997, 10, 10));
        MarqueDto mdto=service.save(dto);
        System.out.println(mdto);
        long id=mdto.getId();
        System.out.println("-------------------------------");
        mdto.setNom("Marque L1");
        mdto=service.update(mdto,id);
        mdto=service.getMarqueById(id);
        System.out.println(mdto);
        System.out.println("-------------------------------");
        System.out.println(service.deleteMarque(id));
        System.out.println(service.deleteMarque(id));
        
    }

}
