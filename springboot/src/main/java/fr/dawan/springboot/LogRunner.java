package fr.dawan.springboot;

import org.springframework.boot.CommandLineRunner;

import lombok.extern.slf4j.Slf4j;

@Slf4j
// Runner => on implémente l'interface ApplicationRunner ou CommandLineRunner et
// la méthode run est exécutée une seule fois par après l’initialisation du context

// @Component
// @Order(3)
public class LogRunner implements CommandLineRunner {

    // private static final Logger log=LoggerFactory.getLogger(LogRunner.class);
    // -> peut-être générer avec l'annotation @Slf4j de lombok
    @Override
    public void run(String... args) throws Exception {
        log.trace("Message TRACE");
        log.debug("Message DEBUG");
        log.info("Message INFO");
        log.warn("Message WARN");
        log.error("Message ERROR");
    }
}
