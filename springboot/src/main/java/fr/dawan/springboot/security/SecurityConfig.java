package fr.dawan.springboot.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    // On définie l'encoder de mot de passe
    @Bean
    PasswordEncoder encoder() {
        // Mot de passe en clair dans la bdd
        // return PasswordEncoderFactories.createDelegatingPasswordEncoder();

        // Mot de passe encoder avec Bcrypt dans la bdd
        return new BCryptPasswordEncoder();
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // Sans les authority
//        http.csrf(c -> c.disable())
//        .authorizeHttpRequests(auth -> auth.requestMatchers(
//                new AntPathRequestMatcher("/**", "POST")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**", "DELETE")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**", "PUT")).authenticated()
//                .anyRequest().permitAll())
//        .httpBasic(Customizer.withDefaults());
//        return http.build();
        
        // Avec les authority
        http.csrf(c -> c.disable())
        .authorizeHttpRequests(auth -> auth.requestMatchers(
                new AntPathRequestMatcher("/**", "POST")).hasAnyAuthority("WRITE", "ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**", "DELETE")).hasAnyAuthority("DELETE", "ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**", "PUT")).hasAnyAuthority("WRITE", "ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**", "GET")).hasAnyAuthority("READ", "ADMIN")
                .anyRequest().permitAll())
        .httpBasic(Customizer.withDefaults());
        return http.build();
    }

}
