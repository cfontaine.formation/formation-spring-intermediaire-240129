package fr.dawan.springboot.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class CharteGraphiqueDto {

    private long id;

    private String couleur;

    private byte[] logo;

}
