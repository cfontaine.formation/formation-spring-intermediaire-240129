package fr.dawan.springboot.dtos;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class MarqueDto {

    private long id;

    private String nom;
    // @JsonIgnore
    // @JsonProperty("creation")
    // @JsonFormat(shape=Shape.STRING,pattern = "dd-MM-YY")
    private LocalDate dateCreation;

}
