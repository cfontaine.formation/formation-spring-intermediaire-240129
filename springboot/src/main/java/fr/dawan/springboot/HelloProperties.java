package fr.dawan.springboot;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Configuration
@ConfigurationProperties(prefix = "hello")
public class HelloProperties {

    private String message;

    private String nom;

    private int age;
}
