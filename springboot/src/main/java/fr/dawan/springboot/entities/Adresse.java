package fr.dawan.springboot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Data;

@Data
// Une classe intégrable va stocker ses données dans la table de l’entité mère ce qui va créer des colonnes supplémentaires

// On annote une classe intégrable avec @Embeddable

@Embeddable
public class Adresse {

    private String rue;

    @Column(length = 60)
    private String ville;

    @Column(length = 15, name = "code_postal")
    private String codePostal;
}
