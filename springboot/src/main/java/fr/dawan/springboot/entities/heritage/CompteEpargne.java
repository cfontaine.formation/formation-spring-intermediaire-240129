package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="compte_epargne")
//@DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;
    
    @Column(name="taux_epargne")
    private double tauxEpargne; 
}
