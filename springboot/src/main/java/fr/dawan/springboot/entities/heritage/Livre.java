package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
public class Livre extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(length = 100, nullable = false)
    private String titre;
    
    @Column(name="annee_sortie")
    private int anneeSortie;
}
