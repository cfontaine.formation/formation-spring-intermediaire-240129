package fr.dawan.springboot.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dtos.ArticleDto;
import fr.dawan.springboot.services.GenericService;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController extends GenericController<ArticleDto, Long> {

    public ArticleController(GenericService<ArticleDto, Long> service) {
        super(service);
    }

}
