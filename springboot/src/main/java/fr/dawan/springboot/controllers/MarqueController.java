package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1/marques")
@Tag(name="Marques",description="L'api des marques")
public class MarqueController {

    @Autowired
    private MarqueService service;

    // localhost:8080/api/v1/marques
    @GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> findAll(){
        return service.getAllMarque(Pageable.unpaged());
    }

    // localhost:8080/api/v1/marques?page=0&size=2
    @GetMapping(params= {"page","size"},produces=MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> findAll(Pageable page) {
        return service.getAllMarque(page);
    }

    // localhost:8080/api/v1/marques/3
    
    @Operation(summary = "Trouver les marques enfonction de leur id",description="retourne une marque",tags="Marques")
    @ApiResponses({
        @ApiResponse(responseCode = "200", description = "Opération réussit" ,content = @Content(schema = @Schema(implementation = MarqueDto.class))),
        @ApiResponse(responseCode = "404", description = "Marque non trouvé") 
    })
    
    @GetMapping(value = "/{id:[0-9]+}",produces= {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<MarqueDto> findById(
            @Parameter(description = "L'id de la marque",required = true,allowEmptyValue = false)
            @PathVariable long id) {
        try {
            return ResponseEntity.ok(service.getMarqueById(id));
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    // localhost:8080/api/v1/marques/marqueA
    @GetMapping(value = "/{nom:[A-Za-z]+}",produces=MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> findByName(@PathVariable String nom) {
        return service.getMarqueByNom(nom);
    }
    
    @DeleteMapping(value="/{id}",produces=MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> delete(@PathVariable long id) {
       if( service.deleteMarque(id)) {
           return new ResponseEntity<>("La marque id= " +id + " a été supprimé",HttpStatus.OK);
       }
       else {
           return new ResponseEntity<>("La marque id= " +id + " n'existe pas",HttpStatus.NOT_FOUND);
       }
    }
    
    @ResponseStatus(code=HttpStatus.CREATED)
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto create(@RequestBody MarqueDto marque) {
        return service.save(marque);
    }
    
    @PutMapping(value="/{id}",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto update(@PathVariable long id, @RequestBody MarqueDto marque) {
        return service.update(marque,id);
    }
    
    @GetMapping("/ioexception")
    public void genIOException() throws IOException {
        throw new IOException("Erreur E/S"); 
    }
    
    @GetMapping("/sqlexception")
    public void genSQLException() throws SQLException {
        throw new SQLException("Erreur bdd"); 
    }
    
    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handlerIoException(IOException e){
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
