package fr.dawan.springboot.controllers;

import java.io.IOException;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.dtos.CharteGraphiqueDto;
import fr.dawan.springboot.services.CharteGraphiqueService;

@RestController
@RequestMapping("/api/v1/chartes")
public class CharteGraphiqueController extends GenericController<CharteGraphiqueDto, Long> {

    private ObjectMapper objectMapper;

    public CharteGraphiqueController(CharteGraphiqueService service, ObjectMapper objectMapper) {
        super(service);
    }

    @PostMapping(value = "/upload/{id}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public CharteGraphiqueDto uploadImage(@PathVariable long id, @RequestParam("image") MultipartFile file)
            throws IOException {
        CharteGraphiqueDto cg = service.getById(id);
        System.out.println(file.getOriginalFilename());
        cg.setLogo(file.getBytes());
        return service.save(cg);
    }

    @PostMapping(consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public CharteGraphiqueDto create(@RequestParam("charte") String charteJson,
            @RequestParam("image") MultipartFile file) throws IOException {
        CharteGraphiqueDto dto = objectMapper.readValue(charteJson, CharteGraphiqueDto.class);
        dto.setLogo(file.getBytes());
        return service.save(dto);
    }

}
