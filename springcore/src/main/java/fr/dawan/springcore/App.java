package fr.dawan.springcore;

import java.time.LocalDate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Article;
import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App {
    public static void main(String[] args) {
        // Exemple lombok
        Article a = new Article("TV", 400.0, LocalDate.of(2020, 10, 10));
        System.out.println(a.getPrix());
        System.out.println(a);

        // Builder
        Article a2 = Article.builder().prix(10.0).description("Stylo").build();
        System.out.println(a2);

        // Spring Core
        // Création du conteneur d'ioc
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);
        System.out.println("-------------------------------");
        
        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DtoMapper m1 = ctx.getBean("mapper1", DtoMapper.class);
        System.out.println(m1);
        
        ArticleRepository repo1 = ctx.getBean("repository1", ArticleRepository.class);
        System.out.println(repo1);
        
        ArticleRepository repo2 = ctx.getBean("repository2", ArticleRepository.class);
        System.out.println(repo2);

        ArticleService serv1 = ctx.getBean("service1", ArticleService.class);
        System.out.println(serv1);
        
        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) ctx).close();
    }
}
