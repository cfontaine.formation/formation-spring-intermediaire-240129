package fr.dawan.monument;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.monument.entities.Monument;
import fr.dawan.monument.repositories.EtiquetteRepository;
import fr.dawan.monument.repositories.MonumentRepository;

@Component
public class RepositoryRunner implements CommandLineRunner {
    
    @Autowired
    private MonumentRepository monumentRepo;
    
    @Autowired
    private EtiquetteRepository etiquetteRepo;

    @Override
    public void run(String... args) throws Exception {
       List<Monument> monuments=monumentRepo.findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(2000, 2024);
        monuments.forEach(m-> System.out.println(m));
        System.out.println("-------------------------------------------");
        monumentRepo.findByNomLike("T%")
        .forEach(m -> System.out.println(m));
        System.out.println("-------------------------------------------");
        Optional<Monument> o=monumentRepo.findByCoordonneLatitudeAndCoordonneLongitude(19.9788889, 31.133888888888887);
        if(o.isPresent()) {
            System.out.println(o.get());
        }
        System.out.println("-------------------------------------------");
        
        monumentRepo.findByLocalisationPaysIn(PageRequest.of(0, 6), "France","Italie","Japon")
        .forEach(m -> System.out.println(m));
        
        monumentRepo.findTop5ByOrderByAnneeConstruction()
        .forEach(m -> System.out.println(m));
        
        monumentRepo.findByEtiquettesIntitule("Exposition universelle")
        .forEach(m -> System.out.println(m));
        
        etiquetteRepo.findByIntituleLike("r%").forEach(m -> System.out.println(m));
    }

}
