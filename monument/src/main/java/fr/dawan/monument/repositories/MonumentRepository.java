package fr.dawan.monument.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Monument;

public interface MonumentRepository extends JpaRepository<Monument, Long> {

    // rechercher les monuments en fonction d'un interval sur les années
    // de construction trie par année de construction décroissant
    List<Monument> findByAnneeConstructionBetweenOrderByAnneeConstructionDesc(int anneeMin, int anneeMax);

    // Rechercher les monuments en fonction d'un modèle(LIKE) sur le nom du monument
    List<Monument> findByNomLike(String modele);
    
    // rechercher les monuments en fonction d'une latitude et d'un longitude
    // Monument findByCoordonneLatitudeAndCoordonneLongitude(double latitude,double longitude);
    Optional<Monument> findByCoordonneLatitudeAndCoordonneLongitude(double latitude,double longitude);

    // rechercher les monuments en fonction de plusieurs pays et de manière paginé
    Page<Monument> findByLocalisationPaysIn(Pageable page,String...  pays);
    
    // rechercher les 5 monuments les plus ancien
    List<Monument> findTop5ByOrderByAnneeConstruction();
    
    // rechercher les monuments en fonction d'un intitulé d’étiquette
    List<Monument> findByEtiquettesIntitule(String intitule);
}
