package fr.dawan.monument.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.monument.entities.Etiquette;

public interface EtiquetteRepository extends JpaRepository<Etiquette, Long> {
  //  Rechercher les étiquettes en fonction d'un modèle(LIKE) sur l'intitule' de l’étiquette
    List<Etiquette> findByIntituleLike(String modele);
}
