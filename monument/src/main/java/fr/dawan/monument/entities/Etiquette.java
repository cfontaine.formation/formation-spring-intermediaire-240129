package fr.dawan.monument.entities;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name="etiquettes")
public class Etiquette extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable=false,length=60)
    private String intitule;
    
    @ManyToMany(mappedBy="etiquettes")
    @Exclude
    private Set<Monument> monuments=new HashSet<>();
}
