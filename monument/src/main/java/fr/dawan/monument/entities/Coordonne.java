package fr.dawan.monument.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name="coordonnees")
public class Coordonne extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(nullable=false)
    private double longitude;
    
    @Column(nullable=false)
    private double latitude;
    
    @OneToOne(mappedBy="coordonne")
    @Exclude
    private Monument monument;

}
