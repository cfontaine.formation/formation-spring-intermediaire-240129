package fr.dawan.monument.entities;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "monuments")
public class Monument extends BaseEntity {
    
    private static final long serialVersionUID = 1L;

    @Column(nullable=false,length=150)
    private String nom;
    
    @Column(nullable=false,name="annee_construction")
    private int anneeConstruction;
    
    private String description;
    
    @Lob
    @Column(length = 65000)
    @Exclude
    private byte[] photo;
    
    @OneToOne
    private Coordonne coordonne;
    
    @ManyToOne
    private Localisation localisation;
    
    @ManyToMany
    @Exclude
    private Set<Etiquette> etiquettes=new HashSet<>();
    
    
}
