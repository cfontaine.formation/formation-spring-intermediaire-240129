package fr.dawan.monument.entities;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name="localisations")
public class Localisation extends BaseEntity {

    private static final long serialVersionUID = 1L;
    
    @Column(nullable=false,length=80)
    private String  pays;
    
    @Column(nullable=false,length=80)
    private String ville;
    
    @OneToMany(mappedBy="localisation")
    @Exclude
    private Set<Monument> monuments=new HashSet<>();
}