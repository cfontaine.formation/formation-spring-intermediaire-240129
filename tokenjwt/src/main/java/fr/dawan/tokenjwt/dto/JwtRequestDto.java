package fr.dawan.tokenjwt.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class JwtRequestDto {
    
    @NotEmpty
    @Size(min=1,max=60)
    private String username;
    
    @NotEmpty
    @Size(min=4,max=40)
    private String password;

}

