package fr.dawan.tokenjwt.dto;

import fr.dawan.tokenjwt.enums.Emballage;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ArticleDto {

    private long id;

    @Positive
    private double prix;

    @NotEmpty
    @Size(min=1,max=255)
    private String description;

    @NotNull
    private Emballage emballage;

}
