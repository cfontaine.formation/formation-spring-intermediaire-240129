package fr.dawan.tokenjwt.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CharteGraphiqueDto {
    private long id;

    @NotEmpty
    @Size(min = 6, max = 6)
    private String couleur;

    private byte[] logo;

}
