package fr.dawan.tokenjwt.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MarqueDto {

    private long id;

    @NotEmpty
    @Size(min = 2, max = 60)
    private String nom;

    @PastOrPresent
    @JsonProperty("creation")
    private LocalDate dateCreation;
}
