package fr.dawan.tokenjwt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.tokenjwt.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque,Long> {

    List<Marque> findByNomLike(String nom);
    
    int removeById(long id);
}
