package fr.dawan.tokenjwt.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.tokenjwt.entities.Article;

@Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {
    
    List<Article> findByDescriptionLike(String modele);
    
    List<Article> findByPrixBetween(double prixMin,double prixMax,Pageable page);
    
    int removeById(long id);
}
