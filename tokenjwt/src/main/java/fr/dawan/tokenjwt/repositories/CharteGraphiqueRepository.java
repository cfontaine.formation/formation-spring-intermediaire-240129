package fr.dawan.tokenjwt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.tokenjwt.entities.CharteGraphique;

public interface CharteGraphiqueRepository extends JpaRepository<CharteGraphique, Long> {

    List<CharteGraphique> findByCouleur(String couleur);
    
    int removeById(long id);
}
