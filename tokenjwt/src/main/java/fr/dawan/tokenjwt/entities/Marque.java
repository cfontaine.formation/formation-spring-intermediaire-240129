package fr.dawan.tokenjwt.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name="marques")
public class Marque extends BaseAuditing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable=false,length=60)
    private String nom;
    
    private LocalDate dateCreation;
    
    @OneToOne
    private CharteGraphique charte;
    
//    @OneToMany/*(mappedBy="marque")*/
//    @JoinColumn(name="marque_id")
    
    @OneToMany(mappedBy="marque",cascade = CascadeType.ALL)
    @Exclude
    private Set<Article> articles=new HashSet<>();
    
}
