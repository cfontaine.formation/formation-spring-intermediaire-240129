package fr.dawan.tokenjwt.entities;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseAuditing {
    
    @CreatedDate
    @Column(updatable = false) // nullable=false
    private LocalDateTime created;
    
    @LastModifiedDate
    @Column(name="last_modified") 
    private LocalDateTime lastModified;
    
    @CreatedBy
    @Column(name="created_by",length = 100,updatable = false) // nullable=false
    private String createdBy;
    
    @LastModifiedBy
    @Column(name="last_modified_by",length = 100)
    private String modifiedBy;

}
