package fr.dawan.tokenjwt.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericService<TDto, ID> {

    Page<TDto> getAll(Pageable page);

    Optional<TDto> getById(ID id);

    boolean delete(ID id);

    TDto save(TDto dto);

    Optional<TDto> update(TDto dto, ID id);
}
