package fr.dawan.tokenjwt.services;

import fr.dawan.tokenjwt.dto.CharteGraphiqueDto;

public interface CharteGraphiqueService extends GenericService<CharteGraphiqueDto, Long>{
    
}
