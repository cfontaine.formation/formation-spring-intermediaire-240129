package fr.dawan.tokenjwt.services;

import fr.dawan.tokenjwt.dto.ArticleDto;

public interface ArticleService extends GenericService<ArticleDto, Long> {

}
