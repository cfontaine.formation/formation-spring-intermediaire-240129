package fr.dawan.tokenjwt.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.tokenjwt.dto.ArticleDto;
import fr.dawan.tokenjwt.entities.Article;
import fr.dawan.tokenjwt.repositories.ArticleRepository;
import fr.dawan.tokenjwt.services.ArticleService;

@Service
@Transactional(readOnly = true)
public class ArticleServiceImpl extends GenericServiceImpl<ArticleDto, Article, Long> implements ArticleService {

    public ArticleServiceImpl(ArticleRepository repository, ModelMapper mapper) {
        super(repository, mapper, Article.class, ArticleDto.class);
    }

    @Override
    public boolean delete(Long id) {
        return ((ArticleRepository) repository).removeById(id) != 0;
    }

    @Override
    protected void updateEntity(Article a, ArticleDto dto) {
        a.setDescription(dto.getDescription());
        a.setEmballage(dto.getEmballage());
        a.setPrix(dto.getPrix());
    }
}
