package fr.dawan.tokenjwt.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.tokenjwt.dto.MarqueDto;
import fr.dawan.tokenjwt.entities.Marque;
import fr.dawan.tokenjwt.repositories.MarqueRepository;
import fr.dawan.tokenjwt.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl extends GenericServiceImpl<MarqueDto, Marque, Long> implements MarqueService {

    public MarqueServiceImpl(MarqueRepository repository, ModelMapper mapper) {
        super(repository, mapper, Marque.class, MarqueDto.class);
    }

    @Override
    public List<MarqueDto> getByNom(String modele) {
        return ((MarqueRepository) repository).findByNomLike(modele).stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    protected void updateEntity(Marque marque, MarqueDto dto) {
        marque.setNom(dto.getNom());
        marque.setDateCreation(dto.getDateCreation());
    }
}
