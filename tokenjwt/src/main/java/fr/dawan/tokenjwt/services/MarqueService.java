package fr.dawan.tokenjwt.services;

import java.util.List;

import fr.dawan.tokenjwt.dto.MarqueDto;

public interface MarqueService extends GenericService<MarqueDto, Long> {

    List<MarqueDto> getByNom(String modele);

}
