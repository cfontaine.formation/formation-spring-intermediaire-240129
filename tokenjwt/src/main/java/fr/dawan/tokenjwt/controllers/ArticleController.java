package fr.dawan.tokenjwt.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.tokenjwt.dto.ArticleDto;
import fr.dawan.tokenjwt.services.GenericService;

@RestController
@RequestMapping("/api/v1/articles")
public class ArticleController extends GenericController<ArticleDto, Long> {

    public ArticleController(GenericService<ArticleDto, Long> service) {
        super(service);
    }

}
