package fr.dawan.tokenjwt.controllers;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.tokenjwt.dto.MarqueDto;
import fr.dawan.tokenjwt.services.MarqueService;

@RestController
@RequestMapping("/api/v1/marques")

public class MarqueController extends GenericController<MarqueDto, Long> {

    public MarqueController(MarqueService service) {
        super(service);
    }

    @GetMapping(value = "/{nom:[A-Za-z]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> findByName(@PathVariable String nom) {
        return ((MarqueService) service).getByNom("%" + nom + "%");
    }

}